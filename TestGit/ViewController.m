//
//  ViewController.m
//  TestGit
//
//  Created by Vladyslav Bedro on 6/12/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

//properties

// methods

@end

@implementation ViewController


#pragma mark - Life cycle -

-  (void) viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Memory managment -

-  (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
